/* eslint no-use-before-define: 2 */

const clientId = 'gepi0rgfmixb4bjjutzxrs5y6x6fql';
const redirectURI = 'http://alwaysnotify.me/';
const scope = 'user_read+chat_login';
let ws;


// Source: https://www.thepolyglotdeveloper.com/2015/03/create-a-random-nonce-string-using-javascript/
function nonce(length) {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < length; i += 1) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

function parseFragment(hash) {
  const hashMatch = (expr) => {
    const match = hash.match(expr);
    return match ? match[1] : null;
  };
  const state = hashMatch(/state=(\w+)/);
  if (sessionStorage.twitchOAuthState === state) {
    sessionStorage.twitchOAuthToken = hashMatch(/access_token=(\w+)/);
  }
}

function authUrl() {
  sessionStorage.twitchOAuthState = nonce(15);
  let url = 'https://api.twitch.tv/kraken/oauth2/authorize';
  url += '?response_type=token';
  url += `&client_id=${clientId}`;
  url += `&redirect_uri=${redirectURI}`;
  url += `&state=${sessionStorage.twitchOAuthState}`;
  url += `&scope=${scope}`;
  return url;
}

function heartbeat() {
  const message = {
    type: 'PING',
  };
  $('.ws-output').append(`SENT: ${JSON.stringify(message)}\n`);
  ws.send(JSON.stringify(message));
}

function listen(topic) {
  const message = {
    type: 'LISTEN',
    nonce: nonce(15),
    data: {
      topics: [topic],
      auth_token: sessionStorage.twitchOAuthToken,
    },
  };
  $('.ws-output').append(`SENT: ${JSON.stringify(message)}\n`);
  ws.send(JSON.stringify(message));
}

function connect() {
  const heartbeatInterval = 1000 * 60; // ms between PING's
  const reconnectInterval = 1000 * 3; // ms to wait before reconnect
  let heartbeatHandle;

  ws = new WebSocket('wss://pubsub-edge.twitch.tv');

  ws.onopen = () => {
    $('.ws-output').append('INFO: Socket Opened\n');
    heartbeat();
    heartbeatHandle = setInterval(heartbeat, heartbeatInterval);
  };

  ws.onerror = (error) => {
    $('.ws-output').append(`ERR: ${JSON.stringify(error)}\n`);
  };

  ws.onmessage = (event) => {
    const message = JSON.parse(event.data);
    $('.ws-output').append(`RECV: ${JSON.stringify(message)}\n`);
    if (message.type === 'RECONNECT') {
      $('.ws-output').append('INFO: Reconnecting...\n');
      setTimeout(connect, reconnectInterval);
    }
  };

  ws.onclose = () => {
    $('.ws-output').append('INFO: Socket Closed\n');
    clearInterval(heartbeatHandle);
    $('.ws-output').append('INFO: Reconnecting...\n');
    setTimeout(connect, reconnectInterval);
  };
}

const defaultHeaders = {
  'Client-ID': clientId,
  Authorization: `OAuth ${sessionStorage.twitchOAuthToken}`,
};

// const pretty = json => JSON.stringify(json, null, 4);
const reducePromise = array => array.reduce((cur, next) => {
  const fn = cur.then(next);
  return fn;
});

const ajax = options => new Promise((resolve, reject) => {
  if (!options.url) throw Error('[Ajax] missing url!');
  $.ajax({
    url: options.url,
    method: options.method || 'GET',
    headers: options.method || defaultHeaders,
  })
    .done(data => resolve(data))
    .fail(execption => reject(execption));
});
const myFollows = [];
$(() => {
  if (document.location.hash.match(/access_token=(\w+)/)) {
    parseFragment(document.location.hash);
  }
  if (sessionStorage.twitchOAuthToken) {
    connect();
    $('.socket').show();
    // ---------------------------
    // authenticate
    // ---------------------------
    ajax({
      url: 'https://api.twitch.tv/kraken/user',
    })
      // ---------------------------
      // get all following
      // ---------------------------
      .then(res => ajax({
        url: `https://api.twitch.tv/helix/users/follows?first=25&from_id=${res._id}`,
      }))
      // ---------------------------
      // get following details
      // ---------------------------
      .then((res) => {
        console.log('follow res', res);
        // ---------------------------
        // build follow array
        // ---------------------------
        const followingAjax = res.data.map((d) => {
          const newD = ajax({
            url: `https://api.twitch.tv/helix/streams?user_id=${d.to_id}`,
          })
            .then((followsRes) => {
              if (followsRes.data) myFollows.push(followsRes);
            });
          return newD;
        });
        return reducePromise(followingAjax);
      })
      .then(() => console.log('My follows:', myFollows))
      .catch(execption => console.error('Oh snap!', execption));
    // ---------------------------
    // get top streams
    // ---------------------------
    ajax({
      url: 'https://api.twitch.tv/helix/streams',
    })
      .then((res) => {
        const data = res.data.map((d) => {
          const newD = {
            id: d.id,
            title: d.title,
          };
          return newD;
        });
        console.log('All streams:', data);
      })
      .catch(execption => console.error('Oh snap!', execption));
  } else {
    const url = authUrl();
    $('#auth-link').attr('href', url);
    $('.auth').show();
  }
});

$('#topic-form').submit((event) => {
  const topic = $('#topic-text').val();
  listen(topic);
  event.preventDefault();
});
