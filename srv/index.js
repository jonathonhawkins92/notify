const globals = require('./modules/globals.js');
const util = require('./modules/util.js');
const logger = require('./modules/logger.js');
const fs = require('fs');
const { promisify } = require('util');

const fsp = {
  readFile: promisify(fs.readFile),
};

const payload = {
  hostname: 'https://api.twitch.tv/helix/webhooks/hub',
  port: 80,
  path: '/upload',
  method: 'POST',
  headers: {
    'Client-ID': '',
    'Content-Type': 'application/json',
  },
  data: {
    'hub.mode': 'subscribe',
    'hub.topic': 'https://api.twitch.tv/helix/users/follows?to_id=1337',
    'hub.callback': 'https://alwaysnotify.me',
    'hub.lease_seconds': '864000',
    'hub.secret': '',
  },
};
fsp.readFile(globals.paths.keys.client.id, 'utf8')
  .then((clientId) => {
    payload.headers['Client-ID'] = clientId;
  })
  .then(() => fsp.readFile(globals.paths.keys.client.secret, 'utf8'))
  .then((clientSecret) => {
    payload.headers['Client-ID'] = util.sha256(clientSecret);
    logger.log(util.pretty(payload));
  })
  .catch(ex => logger.error('[error]', ex));
