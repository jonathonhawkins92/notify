const path = require('path');

const GLOBALS = {
  paths: {
    keys: {
      client: {
        id: path.join(__dirname, '..', 'keys', 'client.id.key'),
        secret: path.join(__dirname, '..', 'keys', 'client.secret.key'),
      },
    },
  },
};


module.exports = GLOBALS;
