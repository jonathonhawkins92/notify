
const chalk = require('chalk');
const moment = require('moment');

class PrivateLogger {
  /**
   * All node consoles go through this point
   * the no console rule doesnt apply to node
   * as I will need an output.
   * @param {string} message
   */
  static execute(message) {
    console.log(message); // eslint-disable-line
  }
  /**
   * @param {array} message the console message
   * @param {string} type the console type
   */
  static clean(message, type) {
    let output = '';
    message.forEach((input) => {
      output += String(input);
    });
    const dateTime = moment().format('YY/MM/DD HH:mm:ss');
    return `[${dateTime} | ${type}] ${output}`;
  }
}

class PublicLogger {
  constructor() {
    this.info('[Logger.constructor]');
    this.cleanMsg = '';
  }
  /**
   * @param {array} msg
   */
  log(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'log');
    const message = chalk.cyan(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * @param {array} msg
   */
  info(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'info');
    const message = chalk.blue(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * @param {array} msg
   */
  warn(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'warn');
    const message = chalk.yellow(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * @param {array} msg
   */
  error(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'error');
    const message = chalk.red(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * @param {array} msg
   */
  success(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'done');
    const message = chalk.green(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * this is for the highlighting of suppressed data
   * @param {array} msg
   */
  suppress(...msg) {
    this.cleanMsg = PrivateLogger.clean(msg, 'suppress');
    const message = chalk.grey(this.cleanMsg);
    PrivateLogger.execute(message);
  }
  /**
   * eye catching line break
   */
  br() {
    this.cleanMsg = '--------------------------------------';
    const message = this.cleanMsg;
    PrivateLogger.execute(message);
  }
  /**
   * a new line to space out consoles
   */
  n() {
    this.cleanMsg = '\n';
    const message = this.cleanMsg;
    PrivateLogger.execute(message);
  }
}

// Maintain a single instance of this
let globalLogger = null;
if (globalLogger === null) {
  globalLogger = new PublicLogger();
}

// Export a process-wide singleton
module.exports = globalLogger;
