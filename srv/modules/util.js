
const crypto = require('crypto');

class Util {
  static sha256(key) {
    return crypto
      .createHash('sha256')
      .update(key)
      .digest('base64');
  }
  static pretty(json) {
    return JSON.stringify(json, null, 4);
  }
}

// Maintain a single instance of this
let globalUtil = null;
if (globalUtil === null) {
  globalUtil = Util;
}
// Export a process-wide singleton
module.exports = globalUtil;
