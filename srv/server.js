const express = require('express');
const bodyParser = require('body-parser');
// create application/json parser
const jsonParser = bodyParser.json();
// init app
const app = express();
// set pots
app.set('port', process.env.PORT || 80);
app.set('host', process.env.HOST || '52.215.19.107');
// server
app.listen(app.get('port'), () => console.log(`Express server running at port: ${app.get('port')}`));
// get res
app.get('/getTwitch', jsonParser, (req, res) => {
  if (!req.body) return res.sendStatus(400);
  res.sendStatus(200);
  console.log(req.url);
  return null;
});

// /getTwitch?
// hub.challenge=5HZYq1a_qf9hpUg0yXJ5eQylGVpq0I9vtkTzvPpq
// hub.lease_seconds=864000
// hub.mode=subscribe
// hub.topic=https%3A%2F%2Fapi.twitch.tv%2Fhelix%2Fstreams%3Fuser_id%3Da
