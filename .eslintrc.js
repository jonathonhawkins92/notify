// https://eslint.org/docs/user-guide/configuring

module.exports = {
  extends: ['airbnb'],
  "env": {
    "browser": true,
    "commonjs": false,
    "es6": true,
    "jquery": true,
    "node": true
  },
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
  },
  "rules": {
    "no-console": 0,
    "no-underscore-dangle": 0,
  }
}
